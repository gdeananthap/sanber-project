export function currency(nominal=""){
    let updated = nominal[nominal.length-1];
    for(var i=1; i<nominal.length; i++){
        if(i%3==0){
            updated = "." + updated;
        }
        updated = nominal[nominal.length-1-i] + updated;
    }
    return "Rp " + updated;
}