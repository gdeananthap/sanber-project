import React, { Component } from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity, TextInput } from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

export default class App extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.logoBar}>
                    <Image source={require('./../assets/logo.png')} style={styles.logo} />
                </View>
                <View style={styles.loginTitle}> 
                    <Text style={{fontFamily:'Roboto', fontWeight:"500", fontSize:30, color:'black'}}>Welcome Back!</Text>
                    <Text style={{fontFamily:'Roboto', fontWeight:"500", fontSize:16, color:'#939393'}}>Log in to your existing account</Text>
                </View>
                <View style={styles.inputBar}>
                    <MaterialIcon name="person" style={{paddingLeft: 17, marginRight:25}}size={26} color="#4594D9" />
                    <TextInput
                        autoCorrect={false}
                        placeholder="Username"
                    />
                </View>
                <View style={styles.inputBar}>
                    <MaterialIcon name="lock" style={{paddingLeft: 17, marginRight:25}}size={26} color="#4594D9" />
                    <TextInput
                        autoCorrect={false}
                        placeholder="Password"
                        secureTextEntry={true}
                    />
                </View>
                <TouchableOpacity style={{marginLeft:226, marginRight:27, marginTop:12, marginBottom:12}}>
                    <Text style={{fontFamily:'Roboto', fontWeight:"bold", fontSize:14, color:'#2866A1'}}>Forgot Password?</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.loginButton} onPress={() => this.props.navigation.navigate('Home')}>
                    <Text style={{ fontFamily:'Roboto', fontWeight:"bold", fontSize:25, color:'#FFFFFF', marginLeft:65}}>Log In</Text>
                </TouchableOpacity>
                <View style={{flexDirection:'row', marginBottom:200}}>
                    <Text style={{fontFamily:'Roboto', fontWeight:"normal", fontSize:18, color:'black'}}>Don't have an account?  </Text>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Register')}>
                        <Text style={{fontFamily:'Roboto', fontWeight:"bold", fontSize:18, color:'#2866A1'}}>Sign Up</Text>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('About')}>
                    <Text style={{fontFamily:'Roboto', fontWeight:"bold", fontSize:18, color:'#2866A1'}}>About</Text>
                </TouchableOpacity>
            </View>
        );
    }
}




const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection:'column',
      alignItems:'center'
    },
    logoBar: {
        width: 510,
        height: 243,
        backgroundColor: "#4594D9",
        borderRadius: 50,
        marginTop : -74,
        marginRight : -90
    },
    logo : {
        marginTop : 121,
        marginBottom : 47,
        marginLeft:20,
        width:375, 
        height:75,
    },
    loginTitle : {
        marginTop:40,
        marginBottom:36
    },
    inputBar: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderWidth:2,
        borderRadius : 30,
        borderColor: "#4594D9",
        width : 331,
        height: 45,
        marginTop:13
    },
    loginButton:{
        backgroundColor:"#4594D9",
        borderRadius:20,
        width:211,
        height:39,
        marginBottom:48
    }
});