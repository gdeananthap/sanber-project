import React, { Component } from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity, TextInput } from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

export default class App extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.registerTitle}> 
                    <Text style={{fontFamily:'Roboto', fontWeight:"500", fontSize:30, color:'black'}}>Let's Get Started!</Text>
                    <Text style={{fontFamily:'Roboto', fontWeight:"500", fontSize:16, color:'#939393', textAlign:'center'}}>Create your account</Text>
                </View>
                <View style={styles.inputBar}>
                    <MaterialIcon name="person" style={{paddingLeft: 17, marginRight:25}}size={26} color="#4594D9" />
                    <TextInput
                        autoCorrect={false}
                        placeholder="Full Name"
                    />
                </View>
                <View style={styles.inputBar}>
                    <MaterialIcon name="person" style={{paddingLeft: 17, marginRight:25}}size={26} color="#4594D9" />
                    <TextInput
                        autoCorrect={false}
                        placeholder="Username"
                    />
                </View>
                <View style={styles.inputBar}>
                    <MaterialIcon name="email" style={{paddingLeft: 17, marginRight:25}}size={26} color="#4594D9" />
                    <TextInput
                        autoCorrect={false}
                        placeholder="Email"
                    />
                </View>
                <View style={styles.inputBar}>
                    <MaterialIcon name="phone-iphone" style={{paddingLeft: 17, marginRight:25}}size={26} color="#4594D9" />
                    <TextInput
                        autoCorrect={false}
                        placeholder="Phone"
                    />
                </View>
                <View style={styles.inputBar}>
                    <MaterialIcon name="lock" style={{paddingLeft: 17, marginRight:25}}size={26} color="#4594D9" />
                    <TextInput
                        autoCorrect={false}
                        placeholder="Password"
                        secureTextEntry={true}
                    />
                </View>
                <View style={styles.inputBar}>
                    <MaterialIcon name="lock" style={{paddingLeft: 17, marginRight:25}}size={26} color="#4594D9" />
                    <TextInput
                        autoCorrect={false}
                        placeholder="Confirm Password"
                        secureTextEntry={true}
                    />
                </View>
                <TouchableOpacity style={styles.registerButton} onPress={() => this.props.navigation.navigate('Home')}>
                    <Text style={{ fontFamily:'Roboto', fontWeight:"bold", fontSize:25, color:'#FFFFFF', textAlign:'center'}}>Create</Text>
                </TouchableOpacity>
                <View style={{flexDirection:'row'}}>
                    <Text style={{fontFamily:'Roboto', fontWeight:"normal", fontSize:18, color:'black'}}>Already have an account? </Text>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
                        <Text style={{fontFamily:'Roboto', fontWeight:"bold", fontSize:18, color:'#2866A1'}}>Log In</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}




const styles = StyleSheet.create({
    container: {
      flexDirection:'column',
      alignItems:'center',
      justifyContent:'center',
      flex:1
    },
    registerTitle : {
        marginBottom:38,
    },
    inputBar: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderWidth:2,
        borderRadius : 30,
        borderColor: "#4594D9",
        width : 331,
        height: 45,
        marginTop:13
    },
    registerButton:{
        backgroundColor:"#4594D9",
        borderRadius:20,
        width:211,
        height:39,
        marginBottom:48,
        marginTop:48
    }
});