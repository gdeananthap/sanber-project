import React, { Component } from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity, TextInput } from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons'

export default class App extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.profileBar}>
                    <View style={styles.profileTitle}>
                        <Text style={{fontFamily:'Roboto', fontWeight:"bold", fontSize:40, color:'white', marginLeft:29, marginTop:70}}>Hi, Shiroe!</Text>
                        <Text style={{fontFamily:'Roboto', fontWeight:"normal", fontSize:14, color:'white', marginLeft:29}}>Where are you going next ?</Text>
                    </View>
                    <TouchableOpacity style={{marginTop:70, marginLeft:60}}>
                        <MaterialIcon name="notifications" size={40} color="white" />
                    </TouchableOpacity>
                    <Image source={require('./../assets/shiroe.jpg')} style={{width:62, height:62, borderRadius:31, marginTop:70, marginLeft:8}} />
                </View>
                <View style={styles.searchBar}>
                    <MaterialIcon name="search" style={{paddingLeft: 17, marginRight:10}}size={26} color="#4594D9" />
                    <TextInput
                        autoCorrect={false}
                        placeholder="Search your destination"
                    />
                </View>
                <TouchableOpacity style={styles.categoryButton} onPress={() => this.props.navigation.navigate('Flight')}>
                    <MaterialIcon name="airplanemode-active" size={125} color="white"/>
                </TouchableOpacity>
                <Text style={{fontFamily:'Roboto', fontWeight:"normal", fontSize:30, color:'black'}}>Flights</Text>
                <TouchableOpacity style={styles.categoryButton}>
                    <MaterialCommunity name="home-city" size={125} color="white" />
                </TouchableOpacity>
                <Text style={{fontFamily:'Roboto', fontWeight:"normal", fontSize:30, color:'black'}}>Hotels</Text>
                <View style={styles.bottomBar}>
                    <TouchableOpacity>
                        <MaterialCommunity name="home" size={40} color="#4594D9" />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <MaterialCommunity name="heart" size={40} color="#4594D9" />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <MaterialCommunity name="newspaper" size={40} color="#4594D9" />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <MaterialIcon name="person" size={40} color="#4594D9" />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}




const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection:'column',
      alignItems:'center',
      color:"#E9EAEF"
    },
    profileBar: {
        width: 510,
        height: 243,
        backgroundColor: "#4594D9",
        borderRadius: 50,
        marginTop : -74,
        marginRight : -90,
        flexDirection:'row',
        justifyContent:'flex-start',
        alignItems:'center',
        marginBottom:-30
    },
    searchBar: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderWidth:2,
        borderRadius : 50,
        borderColor: "#939393",
        width : 375,
        height: 63,
        backgroundColor:'white'
    },
    categoryButton:{
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius : 15,
        backgroundColor: "#4594D9",
        width : 180,
        height: 180,
        marginTop:50
    },
    bottomBar:{
        flexDirection:'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: "white",
        borderWidth:2,
        borderRadius : 10,
        borderColor: "#939393",
        width : 375,
        height: 63,
        marginTop:30
    }
});