import React, { Component } from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity, TextInput, TouchableWithoutFeedback, FlatList, Button } from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import flight from '../flights.json';
import {currency} from '../currency';

class FlightItem extends Component {
    render(){
        let flight = this.props.flight;
        return(
            <View style={styles.flightBox}>
                <View style={styles.halfLeft}>
                    <View style={styles.time}>
                        <View style={styles.timeAirport}>
                            <Text style={{fontFamily:'Roboto', fontWeight:"bold", fontSize:20, color:'black'}}>{flight.departTime}</Text>
                            <Text style={styles.airport}>{flight.departure.airport}</Text>
                        </View>
                        <View style={styles.estimation}>
                            <Text style={{fontFamily:'Roboto', fontWeight:"300", fontSize:10, color:'black'}}>{flight.estimationFlightTime}</Text>
                            <MaterialIcon name="linear-scale" size={20} color='#939393'></MaterialIcon>
                            <Text style={{fontFamily:'Roboto', fontWeight:"300", fontSize:10, color:'black'}}>{flight.status}</Text>
                        </View>
                        <View style={styles.timeAirport}>
                            <Text style={{fontFamily:'Roboto', fontWeight:"bold", fontSize:20, color:'black'}}>{flight.arrivalTime}</Text>
                            <Text style={styles.airport}>{flight.destination.airport}</Text>
                        </View>
                    </View>
                    <Image source={{uri: flight.airlineLogo}} style={{width:107, height:28, marginTop:10, marginLeft:6}}/>
                </View>
                <View style={styles.halfRight}>
                    <Text style={{fontFamily:'Roboto', fontWeight:"bold", fontSize:10, color:'black', marginBottom:10}}>{flight.date}</Text>
                    <View style={styles.priceBox}>
                        <Text style={{fontFamily:'Roboto', fontWeight:"bold", fontSize:16, color:'#5FC5FF'}}>{currency(flight.pricePerPack)}</Text>
                        <Text style={{fontFamily:'Roboto', fontWeight:"bold", fontSize:12, color:'#939393'}}>/pack</Text>
                    </View>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate("Detail", {id: flight.id})}>
                        <Text style={styles.chooseButton}>Choose</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
};

export default class App extends Component {
    
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.title}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                        <MaterialCommunity name="arrow-left" size={40} style={{paddingLeft: 20, color: 'white'}}/>
                    </TouchableOpacity>
                    <Text style={{fontSize: 30, paddingLeft: 100, color: 'white'}}>Flights</Text>
                </View>
                <FlatList
                data={flight.items}
                renderItem={(flight) => <FlightItem flight={flight.item} navigation={this.props.navigation}/>}
                keyExtractor={(item) =>item.id}
                />
                
            </View>
        );
    }
}




const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#E9EAEF',
        alignItems: "center",
    },
    title: {
        height: 150,
        width: 500,
        backgroundColor: '#4594D9',
        borderRadius: 50,
        paddingTop: 75,
        marginLeft: 100,
        marginTop: -75,
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
    },
    flightBox: {
        width:375,
        height:100,
        backgroundColor:"#FFFFFF",
        borderRadius:10,
        marginTop:16,
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    halfLeft:{
        flexDirection:'column',
        justifyContent:'space-around',
        alignItems:'flex-start',
        marginLeft:16
    },
    time:{
        flexDirection:'row',
        justifyContent:'flex-start',
        alignItems:'flex-start',
    },
    timeAirport:{
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'center',
        marginLeft:10,
        marginRight:10
    },
    airport:{
        fontFamily:'Roboto', 
        fontWeight:"normal", 
        fontSize:10, 
        color:'black',
        textAlign:'center',
        backgroundColor:'#E9EAEF',
        borderRadius:5
    },
    estimation:{
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'center',
        marginTop:-10
    },
    halfRight:{
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'center',
        marginRight:16
    },
    chooseButton:{
        fontFamily:'Roboto', 
        fontWeight:"bold", 
        fontSize:16, 
        color:'white',
        textAlign:'center',
        backgroundColor:'#5FC5FF',
        borderRadius:10,
        width:95,
        height:20
    },
    priceBox:{
        flexDirection:'row',
        justifyContent:'flex-start',
        alignItems:'flex-end',
    }
});