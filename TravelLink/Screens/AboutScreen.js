import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons'

export default class About extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.title}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                        <MaterialCommunity name="arrow-left" size={40} style={{paddingLeft: 20, color: 'white'}}/>
                    </TouchableOpacity>
                    <Text style={{fontSize: 30, paddingLeft: 80, color: 'white'}}>About Us</Text>
                </View>
                <View style={styles.body}>
                    <View style={styles.name}>
                        <Text style={{fontSize: 24, color: 'white'}}>Kinantan Arya Bagaspati</Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <Image source={require('../assets/foto_kinantan.png')} style={{width:120, height:250}}/>
                        <View>
                            <Text style={{fontSize: 18, paddingTop: 5, paddingBottom: 5, paddingLeft: 5}}>React Native Developer</Text>
                            <View style={{backgroundColor: '#5FC5FF', width: 123}}>
                                <View style={styles.bodyText}>
                                    <Text style={{fontSize: 20, color: 'white'}}>Contact me:</Text>
                                </View>
                            </View>
                            <View style={{flexDirection: "row", justifyContent: "space-around", height: 76}}>
                                <View style={{alignItems: 'center', justifyContent: "space-around", height: 76}}>
                                    <Image source={require('../assets/instagram.png')} style={{width:50, height:50}}/>
                                    <Text style={{fontSize: 10}}>kinantanbagaspati</Text>
                                </View>
                                <View style={{alignItems: 'center', justifyContent: "space-around", height: 76}}>
                                    <Image source={require('../assets/facebook.png')} style={{width:50, height:50}}/>
                                    <Text style={{fontSize: 10}}>Kinantan Bagaspati</Text>
                                </View>
                            </View>
                            <View style={{backgroundColor: '#5FC5FF', width: 123}}>
                                <View style={styles.bodyText}>
                                    <Text style={{fontSize: 20, color: 'white'}}>See my portfolio:</Text>
                                </View>
                            </View>
                            <View style={{flexDirection: "row", justifyContent: "space-around", height: 76}}>
                                <View style={{alignItems: 'center', justifyContent: "space-around", height: 76}}>
                                    <Image source={require('../assets/gitlab.png')} style={{width:50, height:50}}/>
                                    <Text style={{fontSize: 10}}>@kinantanyolo</Text>
                                </View>
                                <View style={{alignItems: 'center', justifyContent: "space-around", height: 76}}>
                                    <Image source={require('../assets/github.png')} style={{width:50, height:50, borderRadius: 25}}/>
                                    <Text style={{fontSize: 10}}>@kinantanbagaspati</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.body}>
                    <View style={styles.name}>
                        <Text style={{fontSize: 24, color: 'white'}}>Gde Anantha Priharsena</Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <Image source={require('../assets/foto_anantha.jpg')} style={{width:120, height:250}}/>
                        <View>
                            <Text style={{fontSize: 18, paddingTop: 5, paddingBottom: 5, paddingLeft: 5}}>React Native Developer</Text>
                            <View style={{backgroundColor: '#5FC5FF', width: 123}}>
                                <View style={styles.bodyText}>
                                    <Text style={{fontSize: 20, color: 'white'}}>Contact me:</Text>
                                </View>
                            </View>
                            <View style={{flexDirection: "row", justifyContent: "space-around", height: 76}}>
                                <View style={{alignItems: 'center', justifyContent: "space-around", height: 76}}>
                                    <Image source={require('../assets/instagram.png')} style={{width:50, height:50}}/>
                                    <Text style={{fontSize: 10}}>gdeananthap</Text>
                                </View>
                                <View style={{alignItems: 'center', justifyContent: "space-around", height: 76}}>
                                    <Image source={require('../assets/facebook.png')} style={{width:50, height:50}}/>
                                    <Text style={{fontSize: 10}}>Gde Anantha P</Text>
                                </View>
                            </View>
                            <View style={{backgroundColor: '#5FC5FF', width: 123}}>
                                <View style={styles.bodyText}>
                                    <Text style={{fontSize: 20, color: 'white'}}>See my portfolio:</Text>
                                </View>
                            </View>
                            <View style={{flexDirection: "row", justifyContent: "space-around", height: 76}}>
                                <View style={{alignItems: 'center', justifyContent: "space-around", height: 76}}>
                                    <Image source={require('../assets/gitlab.png')} style={{width:50, height:50}}/>
                                    <Text style={{fontSize: 10}}>@gdeananthap</Text>
                                </View>
                                <View style={{alignItems: 'center', justifyContent: "space-around", height: 76}}>
                                    <Image source={require('../assets/github.png')} style={{width:50, height:50, borderRadius: 25}}/>
                                    <Text style={{fontSize: 10}}>@gdeananthap</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
                
            </View>
        )
    }
}
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white',
      alignItems: "center"
    },
    title: {
        height: 150,
        width: 500,
        backgroundColor: '#4594D9',
        borderRadius: 50,
        paddingTop: 75,
        marginLeft: 100,
        marginTop: -75,
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
    },
    body: {
        backgroundColor: '#EBEBEB',
        width: 343,
        height: 284,
        marginTop: 20,
    },
    name: {
        width: 365,
        height: 34,
        backgroundColor: '#5FC5FF',
        marginLeft: -11,
        borderRadius: 17,
        alignItems: "center",
        justifyContent: 'center'
    },
    bodyText: {
        width: 234,
        height: 30,
        backgroundColor: '#5FC5FF',
        borderRadius: 15,
        alignItems: "center",
        justifyContent: 'center'
    },
    bottomTitle: {
        height: 30,
        borderBottomWidth: 1
    },
    bottomContent:{
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        flex: 1
    }
})