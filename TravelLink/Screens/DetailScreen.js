import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import flight from '../flights.json';
import {currency} from '../currency';

export default class Detail extends Component {
    state = {
        count: 0,
        total: 0,
        Taxi: "+",
        Rent: "+"
    }
    render() {
        let id = parseInt(this.props.route.params.id);
        return (
            <View style={styles.container}>
                <View style={styles.title}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                        <MaterialCommunity name="arrow-left" size={40} style={{paddingLeft: 20, color: 'white'}}/>
                    </TouchableOpacity>
                    <Text style={{fontSize: 30, paddingLeft: 80, color: 'white'}}>Flight Detail</Text>
                </View>
                <View style={styles.body}>
                    <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                        <View style={{height: 121, justifyContent: 'space-around'}}>
                            <Text style={{fontSize: 16}}>{flight.items[id].departure.city}</Text>
                            <Text style={{fontSize: 30}}>{flight.items[id].departure.airport}</Text>
                            <Text style={{fontSize: 30}}>{flight.items[id].departTime}</Text>
                            <View style={{flexDirection: 'row'}}>
                                <Image source={require('../assets/calendar.png')} style={{width:30, height:30}}/>
                                <View style={{height: 30, justifyContent: 'space-around'}}>
                                    <Text style={{fontSize: 12}}>{flight.items[id].departure.day}</Text>
                                    <Text style={{fontSize: 12}}>{flight.items[id].departure.date}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{height: 121, justifyContent: 'space-around', alignItems: 'center'}}>
                            <Image source={{uri: flight.items[id].airlineLogo}}  style={{width:105, height:28}}/>
                            <Image source={require('../assets/plane.jpg')} style={{width:50, height:50}}/>
                            <Text style={{fontSize: 20}}>{flight.items[id].estimationFlightTime}</Text>
                        </View>
                        <View style={{height: 121, justifyContent: 'space-around', alignItems: 'flex-end'}}>
                            <Text style={{fontSize: 16}}>{flight.items[id].destination.city}</Text>
                            <Text style={{fontSize: 30}}>{flight.items[id].destination.airport}</Text>
                            <Text style={{fontSize: 30}}>{flight.items[id].arrivalTime}</Text>
                            <View style={{flexDirection: 'row'}}>
                                <View style={{height: 30, justifyContent: 'space-around', alignItems: 'flex-end'}}>
                                    <Text style={{fontSize: 12}}>{flight.items[id].destination.day}</Text>
                                    <Text style={{fontSize: 12}}>{flight.items[id].destination.date}</Text>
                                </View>
                                <Image source={require('../assets/calendar.png')} style={{width:30, height:30}}/>
                            </View>
                        </View>
                    </View>
                    <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                        <Text style={{fontSize: 24, color: '#5FC5FF'}}>{currency(flight.items[id].pricePerPack)}</Text>
                        <Text style={{fontSize: 18, color: '#939393'}}>/pax</Text>
                    </View>
                    <View style={{flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center'}}>
                        <Text style={{fontSize: 18}}>Number of seats:</Text>
                        <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                            <TouchableOpacity onPress={()=>{
                                this.setState({count: this.state.count-1 >= 0 ? this.state.count-1 : 0})
                                this.setState({total: this.state.count-1 >= 0 ? this.state.total - parseInt(flight.items[id].pricePerPack) : this.state.total})
                            }}>
                                <View style={styles.smallButton}>
                                    <Text style={{fontSize: 30, color: '#FFFFFF'}}>-</Text>
                                </View>
                            </TouchableOpacity>
                            <View style={{backgroundColor: '#FFFFFF', width: 57, height: 30, borderRadius: 10, borderWidth: 1, alignItems: 'center'}}>
                                <Text>{this.state.count}</Text>
                            </View>
                            <TouchableOpacity onPress={()=>{
                                this.setState({count: this.state.count+1})
                                this.setState({total: this.state.total + parseInt(flight.items[id].pricePerPack)})
                            }}>
                                <View style={styles.smallButton}>
                                    <Text style={{fontSize: 30, color: '#FFFFFF'}}>+</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                    
                </View>
                <Text style={{fontSize: 20}}>Your extension services:</Text>
                <View style={styles.bottomBody}>
                    <View style={{height: 70, justifyContent: 'space-around', width: 200}}>
                        <Text style={{fontSize: 30}}>Add Taxi</Text>
                        <Text style={{fontSize: 24, color: '#5FC5FF'}}>Rp 50.000</Text>
                    </View>
                    <TouchableOpacity onPress={()=>{
                        if(this.state.Taxi === "+"){
                            this.setState({Taxi: "-"})
                            this.setState({total: this.state.total + 50000})
                        }else{
                            this.setState({Taxi: "+"})
                            this.setState({total: this.state.total - 50000})
                        }
                    }}>
                        <View style={styles.bigButton}>
                            <Text style={{fontSize: 50, color: '#FFFFFF'}}>{this.state.Taxi}</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.bottomBody}>
                    <View style={{height: 70, justifyContent: 'space-around', width: 200}}>
                        <Text style={{fontSize: 30}}>Add Car Rent</Text>
                        <Text style={{fontSize: 24, color: '#5FC5FF'}}>Rp 250.000</Text>
                    </View>
                    <TouchableOpacity onPress={()=>{
                        if(this.state.Rent === "+"){
                            this.setState({Rent: "-"})
                            this.setState({total: this.state.total + 250000})
                        }else{
                            this.setState({Rent: "+"})
                            this.setState({total: this.state.total - 250000})
                        }
                    }}>
                        <View style={styles.bigButton}>
                            <Text style={{fontSize: 50, color: '#FFFFFF'}}>{this.state.Rent}</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <Text style={{fontSize: 20, marginTop: 20}}>Your total for this flight:</Text>
                <View style={styles.bottomBody}>
                    <View style={{height: 70, justifyContent: 'space-around', width: 200}}>
                        <Text style={{fontSize: 24, color: '#5FC5FF'}}>{currency(String(this.state.total))}</Text>
                    </View>
                    <TouchableOpacity onPress={()=>{
                        this.props.navigation.goBack()
                        alert("Transaksi sejumlah " + currency(String(this.state.total)) + " akan ditambahkan ke akun anda")
                    }}>
                        <View style={{
                        backgroundColor: '#5FC5FF',
                        width: 96,
                        height: 50,
                        borderRadius: 20,
                        alignItems: 'center',
                        justifyContent: 'center',
                        paddingBottom: 5}}>
                            <Text style={{fontSize: 30, color: '#FFFFFF'}}>Order</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                
            </View>
        )
    }
}
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#EBEBEB',
      alignItems: "center"
    },
    title: {
        height: 150,
        width: 500,
        backgroundColor: '#4594D9',
        borderRadius: 50,
        paddingTop: 75,
        marginLeft: 100,
        marginTop: -75,
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
    },
    body: {
        backgroundColor: 'white',
        width: 360,
        height: 226,
        marginTop: 20,
        marginBottom: 30,
        borderRadius: 10,
        justifyContent: 'space-around'
    },
    bottomBody: {
        backgroundColor: 'white',
        width: 360,
        height: 70,
        marginTop: 10,
        marginBottom: 10,
        borderRadius: 10,
        justifyContent: 'space-around',
        flexDirection: 'row',
        alignItems: 'center'
    },
    smallButton: {
        backgroundColor: '#5FC5FF',
        width: 30,
        height: 30,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 3
    },
    bigButton: {
        backgroundColor: '#5FC5FF',
        width: 50,
        height: 50,
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 5
    }
})